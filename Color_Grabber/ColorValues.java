
public class ColorValues {
	int r,g,b,w,count;
	int[] colorVals;
	//colorVals = new int[4];
	
	
	
	ColorValues() {
		r=1;
		g=1;
		b=1;
		w=1;
		count=0;
		colorVals = new int[4];
		colorVals[0] = 0;
		colorVals[1] = 0;
		colorVals[2] = 0;
		colorVals[3] = 0;
	}
	
	int[] calculate(int finalCount){
		//called after all colors have been added
		count = finalCount;
		int temp = 0;
		temp = (r/count);
		//System.out.println(temp);
		setColorVals(0,temp);
		
		temp = 0;
		temp = (g/count);
		setColorVals(1,temp);
		
		temp = 0;
		temp = (b/count);
		setColorVals(2,temp);

		if (between(getColorVals(0), 200,255) || between(getColorVals(1), 200,255) || between(getColorVals(2), 200,255)){
			setColorVals(3,230);
		}
		else if (between(getColorVals(0), 125,199) || between(getColorVals(1), 125,199) || between(getColorVals(2), 125,199)){
			setColorVals(3,130);
		}
		else if (between(getColorVals(0), 70,124) || between(getColorVals(1), 70,124) || between(getColorVals(2), 70,124)){
			setColorVals(3,80);
		}
		else if (between(getColorVals(0), 25,69) || between(getColorVals(1), 25,69) || between(getColorVals(2), 25,69)){
			setColorVals(3,30);
		}
		else {
			setColorVals(3,10);
		}	
		return colorVals;
	}
	
	boolean between(int x, int min, int max) {
		  return x >= min && x <= max;
		}
	
	void addColorsTogether(int red, int green, int blue, int count){
		addR(red);
		addG(green);
		addB(blue);
		addCount(count);
	}
	
	
	void addR(int rVal){
	    r += rVal;
	}

	void addG(int gVal) {
	  g += gVal;
	}

	void addB (int bVal){
	      b +=  bVal;
	}

	void setW(int wVal) {
	     w = wVal;
	}
	
	void setR(int rVal){
	    r = rVal;
	}

	void setG(int gVal) {
	  g = gVal;
	}

	void setB (int bVal){
	   b =  bVal;
	}
	
	void setCount(int theCount){
		count = theCount; //ah ah ah
	}
	void addCount(int theCount){
		count += theCount;
	}
	
	int[] getColorVals(){
		return colorVals;
	}
	
	void setColorVals(int idx, int value){
		colorVals[idx] = value;
	}
	
	int getColorVals(int idx){
		return colorVals[idx];
	}
	
	void fillColorVal (){
	     colorVals[0] = r;
	     colorVals[1] = g;
	     colorVals[2] = b;
	     colorVals[3] = w;
	}
	
	void setAllZero(){
		 colorVals[0] = 0;
	     colorVals[1] = 0;
	     colorVals[2] = 0;
	     colorVals[3] = 0;
	     setCount(0);
	     setR(1);
	     setG(1);
	     setB(1);
	     setW(1);
	}
	
}
